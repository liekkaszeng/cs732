### How must the marker setup their Auth0 Application and API to work with your solution?

### What files need to be changed in your solution (and where in those files), so that the marker can use their own Auth0 Application and API?

### What changes did you make to the frontend in order to implement Task One? (i.e. which files did you add / change, and why)?
#### add npm packages
  1. @auth0/auth0-react: integrate `auth0`
#### add config files
  1. config.js
  2. auth_config.json
#### change files
  1. index.js: init `Auth0Provider`
  2. App.js: toolbar refactor 
  3. components/TodoList.js: integrate `auth0`
  4. hooks/useCrud.js: http request'header add `Authorization`

### What changes did you make to the backend in order to implement Task Two?
#### add npm packages
  1. express-jwt
  2. jwks-rsa
#### add files
  1. init-jwt.js: config jwt for integrate `auth0`
#### change files
  1. db/todos-dao.js: `retrieveAllTodos` supports filter data by user
  2. db/todos-schema.js: add `sub` field to store user'sub
  3. routes/api/todos-routes.js: integrate `auth0`

### Comment on how easy / difficult it was to add Auth0 authentication to the webapp, and on the documentation you read. What was easily achievable? What was more difficult than anticipated? How effective was the documentation you read in aiding you? What changes, if any, would you recommend, to the documentation for new developers trying to use it?