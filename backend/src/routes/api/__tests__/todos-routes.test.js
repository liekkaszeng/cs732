import routes from '../todos-routes';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import express from 'express';
import axios from 'axios';
import connectToDatabase from '../../../db/db-connect';
import { Todo } from '../../../db/todos-schema';
import dayjs from 'dayjs';

let mongod, app, server;

jest.setTimeout(100000);
const TEST_PORT = 8888;
const TEST_TOKEN = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkFWdnNTb2poa2M5bFV3b0RFbHFJdSJ9.eyJpc3MiOiJodHRwczovL2xpZWtrYXMudXMuYXV0aDAuY29tLyIsInN1YiI6Imdvb2dsZS1vYXV0aDJ8MTEyNzc1OTA1MDUyMTA3NzQ1NDM4IiwiYXVkIjpbImh0dHBzOi8vbGlla2thcy51cy5hdXRoMC5jb20vYXBpL3YyLyIsImh0dHBzOi8vbGlla2thcy51cy5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNjE5NTg4NTQyLCJleHAiOjE2MTk2NzQ5NDIsImF6cCI6IlhLUGpHSUp3TlpXQTN2Q0JoWFBBaHNFUXNHZ2xTM2xLIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCJ9.Up6uMFTa3sOMGJvTN3EHLhbAP_VxBEiovSoFIR-5e7E2ksebM91Pbz_7vt-1_rSCzKO4DZPpNrSobt3ZxLYa-OLceHlm2mZfk8VjRKQIUfuTjkmtG01KCLu5MMln6CFdxoocowdVWd-40MNwyhRDACt_MEjHU2qLNpjQ1s4S0DYXj1m35bcOppgGcutq-_1xDS1dlUvB-5DJrLFMJBgZCaEaOXKZ6vUypdh8NgK1xogab4m9wI-jjhsWm8XiWEbNCwB1BtKGB0WQwEC7GMuwqaa_rL9G39_aX9VTItmSmMQn7tjWtDcwC2eFAfRmzPB3L-ObqRG7A2oSQh-j8jAQ1Q`;
const TEST_SUB = 'google-oauth2|112775905052107745438';
const ANOTHER_TOKEN = `eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IkFWdnNTb2poa2M5bFV3b0RFbHFJdSJ9.eyJpc3MiOiJodHRwczovL2xpZWtrYXMudXMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDYwODhiZDc3NGZhYzA1MDA3MDI2YjJlYSIsImF1ZCI6WyJodHRwczovL2xpZWtrYXMudXMuYXV0aDAuY29tL2FwaS92Mi8iLCJodHRwczovL2xpZWtrYXMudXMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTYxOTU5NjAwNCwiZXhwIjoxNjE5NjgyNDA0LCJhenAiOiJYS1BqR0lKd05aV0EzdkNCaFhQQWhzRVFzR2dsUzNsSyIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUgZW1haWwifQ.dWCCWY3Yg6wHyByDFyFfCB6PMLhsOBm7LN2_CZlztbqp_g3INZl0nLhWI0q-E-8OTjRWqiSroT_6dHNIdFmSkOog7MaKRCqLyVldxzW5c-aZuBILpyNKaYy9cmB1_Jryt6UuZoE4T_IU1Jm6kqD6qPJkzlb13TRP_3iPatLaCZJ1JFa0OYyR65Jwd6QnObRCpcx9TdLrzmg9DEZLG4ihaprA4a5QQ_ywX6ZnuqK630ytHGa0kFpUyYNRkNNzQzzHL44pLjq3eDeRKzwnFjTdE0U0VSA1k4vid6SrTdHFtUeT5Vd_ZeVhpf7OBBN-tHkD4nNr4H6-TMIWZSarG-2pGA`;

// Some dummy data to test with
const overdueTodo = {
  _id: new mongoose.mongo.ObjectId('000000000000000000000002'),
  title: 'OverdueTitle',
  description: 'OverdueDesc',
  isComplete: false,
  dueDate: dayjs().subtract(1, 'day').format(),
  sub: TEST_SUB,
};

const upcomingTodo = {
  _id: new mongoose.mongo.ObjectId('000000000000000000000003'),
  title: 'UpcomingTitle',
  description: 'UpcomingDesc',
  isComplete: false,
  dueDate: dayjs().add(1, 'day').format(),
  sub: TEST_SUB,
};

const completeTodo = {
  _id: new mongoose.mongo.ObjectId('000000000000000000000004'),
  title: 'CompleteTitle',
  description: 'CompleteDesc',
  isComplete: true,
  dueDate: dayjs().format(),
  sub: TEST_SUB,
};

const dummyTodos = [overdueTodo, upcomingTodo, completeTodo];

// Start database and server before any tests run
beforeAll(async (done) => {
  mongod = new MongoMemoryServer();

  await mongod.getUri().then((cs) => connectToDatabase(cs));

  app = express();
  app.use(express.json());
  app.use('/api/todos', routes);
  server = app.listen(TEST_PORT, done);
});

// Populate database with dummy data before each test
beforeEach(async () => {
  await Todo.insertMany(dummyTodos);
});

// Clear database after each test
afterEach(async () => {
  await Todo.deleteMany({});
});

// Stop db and server before we finish
afterAll((done) => {
  server.close(async () => {
    await mongoose.disconnect();
    await mongod.stop();
    done();
  });
});

it('retrieves all todos successfully', async () => {
  const response = await axios.get(`http://localhost:${TEST_PORT}/api/todos`, {
    headers: { Authorization: `Bearer ${TEST_TOKEN}` },
  });
  expect(response.status).toBe(200);
  const responseTodos = response.data;
  expect(responseTodos.length).toBe(3);

  for (let i = 0; i < responseTodos.length; i++) {
    const responseTodo = responseTodos[i];
    const expectedTodo = dummyTodos[i];

    expect(responseTodo._id.toString()).toEqual(expectedTodo._id.toString());
    expect(responseTodo.title).toEqual(expectedTodo.title);
    expect(responseTodo.description).toEqual(expectedTodo.description);
    expect(responseTodo.isComplete).toEqual(expectedTodo.isComplete);
    expect(dayjs(responseTodo.dueDate)).toEqual(dayjs(expectedTodo.dueDate));
  }
});

it('retrieves a single todo successfully', async () => {
  const response = await axios.get(
    `http://localhost:${TEST_PORT}/api/todos/000000000000000000000003`,
    {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    }
  );
  expect(response.status).toBe(200);

  const responseTodo = response.data;
  expect(responseTodo._id.toString()).toEqual(upcomingTodo._id.toString());
  expect(responseTodo.title).toEqual(upcomingTodo.title);
  expect(responseTodo.description).toEqual(upcomingTodo.description);
  expect(responseTodo.isComplete).toEqual(upcomingTodo.isComplete);
  expect(dayjs(responseTodo.dueDate)).toEqual(dayjs(upcomingTodo.dueDate));
});

it('returns a 404 when attempting to retrieve a nonexistant todo (valid id)', async () => {
  try {
    await axios.get(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000001`,
      {
        headers: { Authorization: `Bearer ${TEST_TOKEN}` },
      }
    );
    fail('Should have thrown an exception.');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(404);
  }
});

it('returns a 400 when attempting to retrieve a nonexistant todo (invalid id)', async () => {
  try {
    await axios.get(`http://localhost:${TEST_PORT}/api/todos/blah`, {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    });
    fail('Should have thrown an exception.');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(400);
    expect(response.data).toBe('Invalid ID');
  }
});

it('Creates a new todo', async () => {
  const newTodo = {
    title: 'NewTodo',
    description: 'NewDesc',
    isComplete: false,
    dueDate: dayjs('2100-01-01').format(),
  };

  const response = await axios.post(
    `http://localhost:${TEST_PORT}/api/todos`,
    newTodo,
    {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    }
  );

  // Check response is as expected
  expect(response.status).toBe(201);
  expect(response.data).toBeDefined();
  const rTodo = response.data;
  expect(rTodo.title).toBe('NewTodo');
  expect(rTodo.description).toBe('NewDesc');
  expect(rTodo.isComplete).toBe(false);
  expect(dayjs(rTodo.dueDate)).toEqual(dayjs('2100-01-01'));
  expect(rTodo._id).toBeDefined();
  expect(response.headers.location).toBe(`/api/todos/${rTodo._id}`);

  // Check that the todo was actually added to the database
  const dbTodo = await Todo.findById(rTodo._id);
  expect(dbTodo.title).toBe('NewTodo');
  expect(dbTodo.description).toBe('NewDesc');
  expect(dbTodo.isComplete).toBe(false);
  expect(dayjs(dbTodo.dueDate)).toEqual(dayjs('2100-01-01'));
});

it('Gives a 400 when trying to create a todo with no title', async () => {
  try {
    const newTodo = {
      description: 'NewDesc',
      isComplete: false,
      dueDate: dayjs('2100-01-01').format(),
    };

    await axios.post(`http://localhost:${TEST_PORT}/api/todos`, newTodo, {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    });
    fail('Should have thrown an exception.');
  } catch (err) {
    // Ensure response is as expected
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(400);

    // Ensure DB wasn't modified
    expect(await Todo.countDocuments()).toBe(3);
  }
});

it('updates a todo successfully', async () => {
  const toUpdate = {
    _id: new mongoose.mongo.ObjectId('000000000000000000000004'),
    title: 'UPDCompleteTitle',
    description: 'UPDCompleteDesc',
    isComplete: false,
    dueDate: dayjs('2100-01-01').format(),
    sub: TEST_SUB,
  };

  const response = await axios.put(
    `http://localhost:${TEST_PORT}/api/todos/000000000000000000000004`,
    toUpdate,
    {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    }
  );

  // Check response
  expect(response.status).toBe(204);

  // Ensure DB was updated
  const dbTodo = await Todo.findById('000000000000000000000004');
  expect(dbTodo.title).toBe('UPDCompleteTitle');
  expect(dbTodo.description).toBe('UPDCompleteDesc');
  expect(dbTodo.isComplete).toBe(false);
  expect(dayjs(dbTodo.dueDate)).toEqual(dayjs('2100-01-01'));
});

it('Uses the path ID instead of the body ID when updating', async () => {
  const toUpdate = {
    _id: new mongoose.mongo.ObjectId('000000000000000000000003'),
    title: 'UPDCompleteTitle',
    description: 'UPDCompleteDesc',
    isComplete: false,
    dueDate: dayjs('2100-01-01').format(),
    sub: TEST_SUB,
  };

  const response = await axios.put(
    `http://localhost:${TEST_PORT}/api/todos/000000000000000000000004`,
    toUpdate,
    {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    }
  );

  // Check response
  expect(response.status).toBe(204);

  // Ensure correct DB entry was updated
  let dbTodo = await Todo.findById('000000000000000000000004');
  expect(dbTodo.title).toBe('UPDCompleteTitle');
  expect(dbTodo.description).toBe('UPDCompleteDesc');
  expect(dbTodo.isComplete).toBe(false);
  expect(dayjs(dbTodo.dueDate)).toEqual(dayjs('2100-01-01'));

  // Ensure incorrect DB entry was not updated
  dbTodo = await Todo.findById('000000000000000000000003');
  expect(dbTodo.title).toBe('UpcomingTitle');
  expect(dbTodo.description).toBe('UpcomingDesc');
  expect(dbTodo.isComplete).toBe(false);
  expect(dayjs(dbTodo.dueDate)).toEqual(dayjs(upcomingTodo.dueDate));
});

it('Gives a 404 when updating a nonexistant todo', async () => {
  try {
    const toUpdate = {
      _id: new mongoose.mongo.ObjectId('000000000000000000000010'),
      title: 'UPDCompleteTitle',
      description: 'UPDCompleteDesc',
      isComplete: false,
      dueDate: dayjs('2100-01-01').format(),
      sub: TEST_SUB,
    };

    await axios.put(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000010`,
      toUpdate,
      {
        headers: { Authorization: `Bearer ${TEST_TOKEN}` },
      }
    );
    fail('Should have returned a 404');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(404);

    // Make sure something wasn't added to the db
    expect(await Todo.countDocuments()).toBe(3);
  }
});

it('Deletes a todo', async () => {
  const response = await axios.delete(
    `http://localhost:${TEST_PORT}/api/todos/000000000000000000000003`,
    {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    }
  );
  expect(response.status).toBe(204);

  // Check db item was deleted
  expect(await Todo.findById('000000000000000000000003')).toBeNull();
});

it("Doesn't delete anything when it shouldn't", async () => {
  const response = await axios.delete(
    `http://localhost:${TEST_PORT}/api/todos/000000000000000000000010`,
    {
      headers: { Authorization: `Bearer ${TEST_TOKEN}` },
    }
  );
  expect(response.status).toBe(204);

  // Make sure something wasn't deleted from the db
  expect(await Todo.countDocuments()).toBe(3);
});

/* ------------------------------- Task Three ------------------------------- */
// step 2:  unauthenticated user call API
it('3.2.1: returns a 401 when retrieve all todos(unauthenticated user)', async () => {
  try {
    await axios.get(`http://localhost:${TEST_PORT}/api/todos`);
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Make sure DB wasn't modified
    expect(await Todo.countDocuments()).toBe(3);
  }
});
it('3.2.2: returns a 401 when retrieves a single todo(unauthenticated user)', async () => {
  try {
    await axios.get(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000003`
    );
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Make sure DB wasn't modified
    expect(await Todo.countDocuments()).toBe(3);
  }
});
it('3.2.3: returns a 401 when creates a new todo(unauthenticated user)', async () => {
  try {
    const newTodo = {
      title: 'NewTodo',
      description: 'NewDesc',
      isComplete: false,
      dueDate: dayjs('2100-01-01').format(),
    };

    await axios.post(`http://localhost:${TEST_PORT}/api/todos`, newTodo);
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Make sure something wasn't added to the db
    expect(await Todo.countDocuments()).toBe(3);
  }
});
it('3.2.4: returns a 401 when updates a todo(unauthenticated user)', async () => {
  try {
    const toUpdate = {
      _id: new mongoose.mongo.ObjectId('000000000000000000000004'),
      title: 'UPDCompleteTitle',
      description: 'UPDCompleteDesc',
      isComplete: false,
      dueDate: dayjs('2100-01-01').format(),
      sub: TEST_SUB,
    };

    await axios.put(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000004`,
      toUpdate
    );
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Make sure something wasn't added to the db
    expect(await Todo.countDocuments()).toBe(3);
  }
});
it('3.2.5: returns a 401 when deletes a todo(unauthenticated user)', async () => {
  try {
    await axios.delete(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000003`
    );
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Check db item wasn't deleted
    expect(await Todo.findById('000000000000000000000003')).toBeDefined();
  }
});

// step 3: an authenticated user operate another user's todos
it('3.3.1: returns a 401 when retrieves a single todo(belong to anthor user)', async () => {
  try {
    await axios.get(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000003`,
      {
        headers: { authorization: `Bearer ${ANOTHER_TOKEN}` },
      }
    );
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Make sure DB wasn't modified
    expect(await Todo.countDocuments()).toBe(3);
  }
});
it('3.3.2: returns a 401 when updates a todo(belong to anthor user)', async () => {
  try {
    const toUpdate = {
      _id: new mongoose.mongo.ObjectId('000000000000000000000004'),
      title: 'UPDCompleteTitle',
      description: 'UPDCompleteDesc',
      isComplete: false,
      dueDate: dayjs('2100-01-01').format(),
      sub: TEST_SUB,
    };

    await axios.put(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000004`,
      toUpdate,
      {
        headers: { authorization: `Bearer ${ANOTHER_TOKEN}` },
      }
    );
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Make sure something wasn't added to the db
    expect(await Todo.countDocuments()).toBe(3);
  }
});
it('3.3.3: returns a 401 when deletes a todo(belong to anthor user)', async () => {
  try {
    await axios.delete(
      `http://localhost:${TEST_PORT}/api/todos/000000000000000000000003`,
      {
        headers: { authorization: `Bearer ${ANOTHER_TOKEN}` },
      }
    );
    fail('Should have returned a 401');
  } catch (err) {
    const { response } = err;
    expect(response).toBeDefined();
    expect(response.status).toBe(401);

    // Check db item wasn't deleted
    expect(await Todo.findById('000000000000000000000003')).toBeDefined();
  }
});
